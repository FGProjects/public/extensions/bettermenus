-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--
windowParent = nil
function registerWindowParent(myParent)
    Debug.console('BETTERMENU: registerWindowParent')
    windowParent = myParent;
end

function minimizeMe()
    Debug.console('BETTERMENU: minimizeMe')
    windowParent.windowDropDown.minimize();
end

function onHover(bOver)
    Debug.console('BETTERMENU: onHover')
    if not bOver then minimizeMe(); end
end
