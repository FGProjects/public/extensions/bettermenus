-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--


local funcOriginalInitialization;

aAddMenuList = {}
menusWindow = nil;
function onInit()

  DesktopManager.getSidebarDockWidth = getSidebarDockWidth_bm;

  Comm.registerSlashHandler("preferences", processPreferences);
  Comm.registerSlashHandler("settings", processPreferences);
  Comm.registerSlashHandler("options", processPreferences);

  OptionsManager.registerOption2("OPTIONS_MENU", true, "option_header_client", "option_label_OPTION_MENU", "option_entry_cycler", 
  { labels = "option_val_sidebar", values = "sidebar", baselabel = "option_val_menus", baseval = "menus", default = "menus" });

  OptionsManager.registerCallback("OPTIONS_MENU", updateMenuStyle);
end

--[[
  Override this to deal with sidebar mess.
]]
function getSidebarDockWidth_bm()
	-- override piece
	local bMenuStyle = (OptionsManager.getOption("OPTIONS_MENU") == 'menus' or OptionsManager.getOption("OPTIONS_MENU") == '');
	--  need these
	local _nSidebarVisibility = DesktopManager.getSidebarVisibilityState();
	local _szDockCategory = DesktopManager.getSidebarDockCategorySize();
	local _rcDockCategoryOffset = DesktopManager.getSidebarDockCategoryOffset();
	local _rcDockButtonOffset = DesktopManager.getSidebarDockButtonOffset();
	local _szDockButton = DesktopManager.getSidebarDockButtonSize();
	if (bMenuStyle) then
		_nSidebarVisibility = 4;
	end

	hideMenuBar();

	-- end override piece
	if _nSidebarVisibility <= 0 then
		local nDockCategoryWidth = _szDockCategory.w + (_rcDockCategoryOffset.left + _rcDockCategoryOffset.right);
		local nDockButtonWidth = _szDockButton.w + (_rcDockButtonOffset.left + _rcDockButtonOffset.right);
		return math.max(nDockCategoryWidth, nDockButtonWidth);
	end
	local nDockIconWidth = DesktopManager.getSidebarDockIconWidth();
	if _nSidebarVisibility == 1 then
		return nDockIconWidth * 2;
	end
	-- override piece
	if (_nSidebarVisibility == 4) then
		nDockIconWidth = -9;
	end
	-- end override piece
	return nDockIconWidth;
end


--[[
  This will add a custom menu/window item to the Menu button.
  
  sRecord           The window class
  sPath             The database node associated with this window (if any)
  sToolTip          The tooltip string record, Interface.getString(sTooltip); Displayed when someone hovers over the menu selection
  sButtonCustomText The text used as the name for the menu. If doesn't exist will look for "library_recordtype_label_" .. sRecord
]]
function addMenuItem(sRecord, sPath, sToolTip, sButtonCustomText)
  local rMenu = {}
  
  rMenu.sRecord           = sRecord;
  rMenu.sPath             = sPath;
  rMenu.sToolTip          = sToolTip
  rMenu.sButtonCustomText = sButtonCustomText;

  table.insert(aAddMenuList,rMenu);
  -- if the menusWindow is already active then
  -- we destroy the list and rebuild adding
  -- the menu just added
  if menusWindow then
    menusWindow.createMenuSelections();
  end
end

function registerMenusWindow(wList)
  menusWindow = wList;
end

function processPreferences()
  Interface.openWindow("options","");
end

wMenuWindow = nil;
function registerWindowMenu(wWindow)
  if not wMenuWindow then
    wMenuWindow = wWindow;
    updateMenuStyle();
  end
end
wSidebarWindow = nil;
function registerWindowSidebar(wWindow)
  if not wSidebarWindow then
    wSidebarWindow = wWindow;
    updateMenuStyle();
  end
end

function updateMenuStyle()
  if wMenuWindow and wSidebarWindow then
    local bMenuStyle = (OptionsManager.getOption("OPTIONS_MENU") == 'menus' or OptionsManager.getOption("OPTIONS_MENU") == '');

    if bMenuStyle then
      enableMenuStyleButtons();
    else
      enableMenuStyleSidebar();
    end
    DesktopManager.saveSidebarVisibilityState()
  end
end

function enableMenuStyleButtons()
    DesktopManager.setSidebarVisibilityState(4);
    DesktopManager.updateSidebarAnchorWindowPosition();
    wMenuWindow.setPosition(3,5, 0, false);
  end
  --[[
    This hides the Menu button bar if its not being used.
  ]]
  function hideMenuBar()
    local bMenuStyle = (OptionsManager.getOption("OPTIONS_MENU") == 'menus' or OptionsManager.getOption("OPTIONS_MENU") == '');
    if (wMenuWindow and not bMenuStyle) then
      wMenuWindow.setPosition(-90000,-90000, 0, false);
    end
  end
  function enableMenuStyleSidebar()
    hideMenuBar();
    DesktopManager.setSidebarVisibilityState(0);
    DesktopManager.updateSidebarAnchorWindowPosition();
  end
  